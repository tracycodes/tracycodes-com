import React from 'react'
import { css } from '@emotion/core'
import Layout from 'components/Layout'
import Link from 'components/Link'
import Container from 'components/Container'
import { rhythm } from 'lib/typography'
import { graphql } from 'gatsby'


export default ({ data: { site } }) => (
  <Layout site={site}>
    <Container
      css={css`
        padding-bottom: 0;
      `}
    >
      <div
        css={css`
          margin-botton: 40px;
        `}
      >
        <h1
          css={css({
            marginBottom: rhythm(0.3),
            transition: 'all 150ms ease',
          })}
        >
          About
        </h1>
        <p>
          Hello! My name is Tracy Adams, and I'm helping build the future of
          Ecommerce.
        </p>

        <p>
          Over the last 5 years, I've helped develop applications ranging from
          custom education platforms with Vue and Firebase to Shopify Sites with
          extensive custom apps on AWS to basic static sites{' '}
          <em>like this one.</em>
        </p>

        <p>
          With that experience, I've realized that helping Ecommerce merchants
          and store owners customize, extend, and build dream solutions is what
          I really dig. So for now, I'm focusing on helping improve the world of
          Ecommerce with new Headless Shopify websites, working on custom
          backend platform solutions, and contributing to open source projects
          that will help Ecommerce developers worldwide.
        </p>

        <h2>My tech stack</h2>

        <p>
          Below is my <em>preferred</em> tech. I am open to learning new tech if
          it's the better solution, but I like to focus on what provides the
          best experience for me.
        </p>

        <ul>
          <li>Node.js - most of my work revolves around Node.js</li>
          <li>GraphQL - my favorite query langauage</li>
          <li>Golang - for when I need high performance in small packages</li>
          <li>React & Vue - to structure my web apps</li>
          <li>Tailwindcss - to turn my web apps from skeletons into art</li>
          <li>AWS - most of my serverless work is on here</li>
          <li>Now & Netlify - for easy deployments and monitoring</li>
          <li>Shopify - set and forget Ecommerce</li>
        </ul>

        <h2>Work with me</h2>

        <p>
          If you'd like examples of my work, please feel free to reach out to me
          via <Link to="https://twitter.com/tracy_codes">Twitter DM</Link> and
          we can chat!
        </p>

        <p>
          <Link to="/work-with-me">Want to work with me?</Link>
        </p>
      </div>
    </Container>
  </Layout>
)

export const pageQuery = graphql`
  query {
    site {
      ...site
      siteMetadata {
        title
      }
    }
  }
`
