import React from 'react'
import { css } from '@emotion/core'
import Layout from 'components/Layout'
import Container from 'components/Container'
import { rhythm } from 'lib/typography'
import { graphql } from 'gatsby'

export default ({ data: { site } }) => (
  <Layout site={site}>
    <Container
      css={css`
        padding-bottom: 0;
      `}
    >
      <div
        css={css`
          margin-botton: 40px;
        `}
      >
        <h1
          css={css({
            marginBottom: rhythm(0.3),
            transition: 'all 150ms ease',
          })}
        >
          Under construction <span role="img" aria-label="construction symbol" >🚧</span>
        </h1>
        <p>This page is still a work in progress. Subscribe to my newsletter to know when it's done!</p>
      </div>
    </Container>
  </Layout>
)

export const pageQuery = graphql`
  query {
    site {
      ...site
      siteMetadata {
        title
      }
    }
  }
`
