import React from 'react'
import { Link } from 'gatsby'

export default () => {
  return (
    <React.Fragment>
      <Link to="/blog" activeClassName="" aria-label="View blog page">
        Blog
      </Link>
      <Link to="/about" activeClassName="" aria-label="View blog page">
        About
      </Link>
      <Link to="/coming-soon" activeClassName="" aria-label="View blog page">
        Work with me
      </Link>

    </React.Fragment>
  )
}
