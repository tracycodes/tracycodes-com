# TracyCodes Website

This is my personal website. This website contains the following:

- My blog
- My work
- How to work with me

This website is built with the following tech stack

- Frontend: GatsbyJS
- Content: Markdown files
- Deployment: Now
