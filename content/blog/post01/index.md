---
slug: hello-world
date: 2019-09-03
title: 'Hello World'
description: 'Hello World'
published: true
---

How exciting! This is my very first blog post (ever). Don't try Googling it, becuase it's definitely true.

You're probably thinking, "what's this blog about?". To that, I'd like to respond with the following:

I'm primarily starting this blog so I can share the knowledge I've amassaed over the last 4 years of professional web and software development. I've built distributed applications with both [Go](https://golang.org/) and [Node.js](https://nodejs.org/en/), complete desktop apps, warehouse management solutions, and more.

Over the last 4 years, I've spent my time at

1. Agencies
2. Full time freelancing
3. Running an E-Commerce business
4. Working full time as a lead developer for one company

Along with technical know-how, based on my success in the freelancing world, I have gained a lot of knowledge with respect to being successful in the shark-infested waters that is freelance web development and software engineering.

If you're curious, I'm branching out and have rebuilt my entire website using [GatsbyJS](https://www.gatsbyjs.org/) and plan to do some pretty neat things once I get out of this boilerplate stage.

I hope you follow along on [Twitter](https://twitter.com/tracy_codes) and [Instagram](https://instagram.com/tracycodes) as I'll be posting updates there when I push a new post live 🤓