---
slug: where-did-my-october-go-2019
date: 2019-11-10
title: 'Where did my October go?'
description: 'Wow... how in the hell did it already get to be November? I guess I should recap what has been going on.'
published: true
banner: './undraw_time_management_30iu.png'
---

Wow... how in the hell did it already get to be November? I guess I should recap what has been going on.

I spent a lot of my October getting things prepped and ready to go for Black Friday/Cyber Monday at my day job. This included things like building out new product launchs, preparing new site features to drive higher conversion rates, and scheduling the obvious price discounts across our websites.

To give some insights on what I've been working on, here's a glimpse into the specifics:
- Increased performance by over 20% (real-world load times reduced by ~3-4s on average)
- New checkout options to reduce stress of pricing
- New landing pages (always gotta build dem landing pages)
- Some secret projects I'm unfortunately not able to discuss publicly (*yet*)

Along with that, I've taken on some contracting work in the Shopify world. This is awesome because I'm working with up-and-coming tech to build futuristic Ecommerce stores. With the knowledge from Web Dev Twitter, Shopify's GraphQL API Documentation, and my own trial-and-error, I've started work on a few Headless Shopify websites that'll be really nifty! When these projects are done, I'll do a writeup of the tech stacks used, performance vs. the old client websites, and reasons for why I did headless for these use cases.

*ALONG* with the Shopify contract work, I've started work on my first SaaS/Startup as well! 🎉🎉🎉

This will be a service for Ecommerce businesses to better manage their customer service processes. I'm not gonna get into the nitty-gritty of this, because I don't want to give away my product just yet, but just know that the product already exists, and I've done extensive research and have definite validation for what I'm building out.

I'm giving myself a deadline of 10 weeks to get this launched in the Beta phase to start testing with a few select customers. At the end of each week, I'll be posting a review of my week to hold myself accountable to the public. I'm dedicated to releasing this product, as I want to help reduce friction in the Ecommerce space with specific products that I've been experiencing for years.

If you're interested in working with me on this project, I am looking for partners/contributors. Please feel free to [DM me on Twitter](https://twitter.com/tracy_codes) or email me at [tracy@tracycodes.com](mailto:tracy@tracycodes.com).