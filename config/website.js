module.exports = {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/blog your pathPrefix should be "blog"
  siteTitle: 'Tracy Codes', // Navigation and Site Title
  siteTitleAlt: 'Personal website of Tracy Adams', // Alternative Site title for SEO
  siteTitleShort: 'TracyCodes', // short_name for manifest
  siteUrl: 'https://tracycodes.com', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  siteLogo: 'images/logo.png', // Used for SEO and manifest, path to your image you placed in the 'static' folder
  siteDescription: 'This is where I post my thoughts, projects, and how info for how we can work together!',
  author: 'Tracy Adams', // Author for schemaORGJSONLD
  organization: 'Simpl Solutions',

  // siteFBAppID: '123456789', // Facebook App ID - Optional
  userTwitter: '@tracy_codes', // Twitter Username
  ogSiteName: 'TracyCodes | Web Dev Blogging - Notes - MY work', // Facebook Site Name
  ogLanguage: 'en_US',
  googleAnalyticsID: 'UA-151745761-1',

  // Manifest and Progress color
  themeColor: '#5348FF',
  backgroundColor: '#2b2e3c',

  // Social component
  twitter: 'https://twitter.com/tracy_codes/',
  twitterHandle: '@tracy_codes',
  github: 'https://github.com/tracy-codes/',
  linkedin: '',
}
